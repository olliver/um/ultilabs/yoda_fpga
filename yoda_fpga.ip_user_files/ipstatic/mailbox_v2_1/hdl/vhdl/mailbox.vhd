-------------------------------------------------------------------------------
-- mailbox.vhd - Entity and architecture
-------------------------------------------------------------------------------
--
-- (c) Copyright 2001-2013 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and 
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------
-- Filename:        mailbox.vhd
--
-- Description:     
--                  
-- VHDL-Standard:   VHDL'93
-------------------------------------------------------------------------------
-- Structure:   
--              mailbox.vhd
--
-------------------------------------------------------------------------------
-- Author:          rikardw
--
-- History:
--   rikardw  2006-10-19    First Version
--   stefana  2012-12-14    Removed legacy interfaces
--
-------------------------------------------------------------------------------
-- Naming Conventions:
--      active low signals:                     "*_n"
--      clock signals:                          "clk", "clk_div#", "clk_#x" 
--      reset signals:                          "rst", "rst_n" 
--      generics:                               "C_*" 
--      user defined types:                     "*_TYPE" 
--      state machine next state:               "*_ns" 
--      state machine current state:            "*_cs" 
--      combinatorial signals:                  "*_com" 
--      pipelined or register delay signals:    "*_d#" 
--      counter signals:                        "*cnt*"
--      clock enable signals:                   "*_ce" 
--      internal version of output port         "*_i"
--      device pins:                            "*_pin" 
--      ports:                                  - Names begin with Uppercase 
--      processes:                              "*_PROCESS" 
--      component instantiations:               "<ENTITY_>I_<#|FUNC>
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mailbox is
  generic (
    -- General.
    C_FAMILY                : string                 := "virtex7";
    C_EXT_RESET_HIGH        : integer                := 1;
    C_ENABLE_BUS_ERROR      : integer                := 0;
    
    -- AXI4 interface #0 specific.
    C_S0_AXI_BASEADDR       : std_logic_vector       := X"FFFF_FFFF";
    C_S0_AXI_HIGHADDR       : std_logic_vector       := X"0000_0000";
    C_S0_AXI_ADDR_WIDTH     : integer                := 32;
    C_S0_AXI_DATA_WIDTH     : integer                := 32;

    -- AXI4 interface #1 specific.
    C_S1_AXI_BASEADDR       : std_logic_vector       := X"FFFF_FFFF";
    C_S1_AXI_HIGHADDR       : std_logic_vector       := X"0000_0000";
    C_S1_AXI_ADDR_WIDTH     : integer                := 32;
    C_S1_AXI_DATA_WIDTH     : integer                := 32;

    -- AXI4-Stream interface #0 specific.
    C_M0_AXIS_DATA_WIDTH    : integer                := 32;
    C_S0_AXIS_DATA_WIDTH    : integer                := 32;

    -- AXI4-Stream interface #1 specific.
    C_M1_AXIS_DATA_WIDTH    : integer                := 32;
    C_S1_AXIS_DATA_WIDTH    : integer                := 32;

    -- Mailbox specific.
    C_ASYNC_CLKS            : integer                := 0;
    C_NUM_SYNC_FF           : integer                := 2;
    C_IMPL_STYLE            : integer                := 0;
    C_INTERCONNECT_PORT_0   : integer                := 1;
    C_INTERCONNECT_PORT_1   : integer                := 1;
    C_MAILBOX_DEPTH         : integer                := 16
  );
  port (
    -- Common reset signal.
    SYS_Rst           : in  std_logic;

    -- AXI4-Lite interface #0 slave signals.
    S0_AXI_ACLK       : in  std_logic;
    S0_AXI_ARESETN    : in  std_logic;
    S0_AXI_AWADDR     : in  std_logic_vector(C_S0_AXI_ADDR_WIDTH-1 downto 0);
    S0_AXI_AWVALID    : in  std_logic;
    S0_AXI_AWREADY    : out std_logic;
    S0_AXI_WDATA      : in  std_logic_vector(C_S0_AXI_DATA_WIDTH-1 downto 0);
    S0_AXI_WSTRB      : in  std_logic_vector((C_S0_AXI_DATA_WIDTH/8)-1 downto 0);
    S0_AXI_WVALID     : in  std_logic;
    S0_AXI_WREADY     : out std_logic;
    S0_AXI_BRESP      : out std_logic_vector(1 downto 0);
    S0_AXI_BVALID     : out std_logic;
    S0_AXI_BREADY     : in  std_logic;
    S0_AXI_ARADDR     : in  std_logic_vector(C_S0_AXI_ADDR_WIDTH-1 downto 0);
    S0_AXI_ARVALID    : in  std_logic;
    S0_AXI_ARREADY    : out std_logic;
    S0_AXI_RDATA      : out std_logic_vector(C_S0_AXI_DATA_WIDTH-1 downto 0);
    S0_AXI_RRESP      : out std_logic_vector(1 downto 0);
    S0_AXI_RVALID     : out std_logic;
    S0_AXI_RREADY     : in  std_logic;
    
    -- AXI4-Lite interface #1 slave signals.
    S1_AXI_ACLK       : in  std_logic;
    S1_AXI_ARESETN    : in  std_logic;
    S1_AXI_AWADDR     : in  std_logic_vector(C_S1_AXI_ADDR_WIDTH-1 downto 0);
    S1_AXI_AWVALID    : in  std_logic;
    S1_AXI_AWREADY    : out std_logic;
    S1_AXI_WDATA      : in  std_logic_vector(C_S1_AXI_DATA_WIDTH-1 downto 0);
    S1_AXI_WSTRB      : in  std_logic_vector((C_S1_AXI_DATA_WIDTH/8)-1 downto 0);
    S1_AXI_WVALID     : in  std_logic;
    S1_AXI_WREADY     : out std_logic;
    S1_AXI_BRESP      : out std_logic_vector(1 downto 0);
    S1_AXI_BVALID     : out std_logic;
    S1_AXI_BREADY     : in  std_logic;
    S1_AXI_ARADDR     : in  std_logic_vector(C_S1_AXI_ADDR_WIDTH-1 downto 0);
    S1_AXI_ARVALID    : in  std_logic;
    S1_AXI_ARREADY    : out std_logic;
    S1_AXI_RDATA      : out std_logic_vector(C_S1_AXI_DATA_WIDTH-1 downto 0);
    S1_AXI_RRESP      : out std_logic_vector(1 downto 0);
    S1_AXI_RVALID     : out std_logic;
    S1_AXI_RREADY     : in  std_logic;
    
    -- AXI4-Stream interface #0 signals.
    M0_AXIS_ACLK      : in  std_logic;
    M0_AXIS_TLAST     : out std_logic;
    M0_AXIS_TDATA     : out std_logic_vector(C_M0_AXIS_DATA_WIDTH-1 downto 0);
    M0_AXIS_TVALID    : out std_logic;
    M0_AXIS_TREADY    : in  std_logic;
    
    S0_AXIS_ACLK      : in  std_logic;
    S0_AXIS_TLAST     : in  std_logic;
    S0_AXIS_TDATA     : in  std_logic_vector(C_S0_AXIS_DATA_WIDTH-1 downto 0);
    S0_AXIS_TVALID    : in  std_logic;
    S0_AXIS_TREADY    : out std_logic;
    
    -- AXI4-Stream interface #1 signals.
    M1_AXIS_ACLK      : in  std_logic;
    M1_AXIS_TLAST     : out std_logic;
    M1_AXIS_TDATA     : out std_logic_vector(C_M1_AXIS_DATA_WIDTH-1 downto 0);
    M1_AXIS_TVALID    : out std_logic;
    M1_AXIS_TREADY    : in  std_logic;
    
    S1_AXIS_ACLK      : in  std_logic;
    S1_AXIS_TLAST     : in  std_logic;
    S1_AXIS_TDATA     : in  std_logic_vector(C_S1_AXIS_DATA_WIDTH-1 downto 0);
    S1_AXIS_TVALID    : in  std_logic;
    S1_AXIS_TREADY    : out std_logic;
    
    -- Interrupt signals for each side.
    Interrupt_0       : out std_logic;
    Interrupt_1       : out std_logic
  );
end entity mailbox;


library Unisim;
use Unisim.vcomponents.all;

library mailbox_v2_1_6;
use mailbox_v2_1_6.All;


architecture IMP of mailbox is

  -----------------------------------------------------------------------------
  -- Function declaration
  -----------------------------------------------------------------------------
  function Log2(x : integer) return integer is
    variable i : integer := 0;
  begin
    if x = 0 then return 0;
    else

      while 2**i < x loop
        i := i+1;
      end loop;
      return i;
    end if;
  end function Log2;
  
  
  -----------------------------------------------------------------------------
  -- Constant declaration
  -----------------------------------------------------------------------------
  constant C_FIFO_LENGTH_WIDTH     : integer := Log2(C_MAILBOX_DEPTH);
  
  constant C_USE_AXI0              : boolean := ( C_INTERCONNECT_PORT_0 = 2 );
  constant C_USE_AXIS0             : boolean := ( C_INTERCONNECT_PORT_0 = 4 );
  constant C_USE_BUS0              : boolean := C_USE_AXI0;
  constant C_USE_STREAM0           : boolean := C_USE_AXIS0;
  
  constant C_USE_AXI1              : boolean := ( C_INTERCONNECT_PORT_1 = 2 );
  constant C_USE_AXIS1             : boolean := ( C_INTERCONNECT_PORT_1 = 4 );
  constant C_USE_BUS1              : boolean := C_USE_AXI1;
  constant C_USE_STREAM1           : boolean := C_USE_AXIS1;

  constant C_FSL_DWIDTH            : integer := 32;  -- Assume AXIS data width is 32

  -----------------------------------------------------------------------------
  -- Component declaration
  -----------------------------------------------------------------------------
  component if_decode is
    generic (
      -- General generics.
      C_FAMILY                : string                 := "virtex7";
      C_ENABLE_BUS_ERROR      : integer                := 0;
      C_FIFO_LENGTH_WIDTH     : integer                := 4;
      C_INTERCONNECT          : integer                := 1;

      -- FSL generics
      C_FSL_DWIDTH            : integer                := 32;

      -- AXI4-Lite slave generics
      C_S_AXI_BASEADDR        : std_logic_vector       := X"FFFF_FFFF";
      C_S_AXI_HIGHADDR        : std_logic_vector       := X"0000_0000";
      C_S_AXI_ADDR_WIDTH      : integer                := 32;
      C_S_AXI_DATA_WIDTH      : integer                := 32
    );
    port (
      Rst              : in std_logic;

      -- AXI4-Lite SLAVE SINGLE INTERFACE
      S_AXI_ACLK       : in  std_logic;
      S_AXI_ARESETN    : in  std_logic;
      S_AXI_AWADDR     : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_AWVALID    : in  std_logic;
      S_AXI_AWREADY    : out std_logic;
      S_AXI_WDATA      : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_WSTRB      : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
      S_AXI_WVALID     : in  std_logic;
      S_AXI_WREADY     : out std_logic;
      S_AXI_BRESP      : out std_logic_vector(1 downto 0);
      S_AXI_BVALID     : out std_logic;
      S_AXI_BREADY     : in  std_logic;
      S_AXI_ARADDR     : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_ARVALID    : in  std_logic;
      S_AXI_ARREADY    : out std_logic;
      S_AXI_RDATA      : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_RRESP      : out std_logic_vector(1 downto 0);
      S_AXI_RVALID     : out std_logic;
      S_AXI_RREADY     : in  std_logic;
      
      -- Information signals.
      FSL_M_Length     : in  std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
      FSL_S_Length     : in  std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
      Interrupt        : out std_logic;
      
      -- FSL master/slave interface signals.
      FSL_M_Clk        : out std_logic;
      FSL_M_Data       : out std_logic_vector(0 to C_FSL_DWIDTH-1);
      FSL_M_Control    : out std_logic;
      FSL_M_Write      : out std_logic;
      FSL_M_Full       : in  std_logic;
      
      FSL_S_Clk        : out std_logic;
      FSL_S_Data       : in  std_logic_vector(0 to C_FSL_DWIDTH-1);
      FSL_S_Control    : in  std_logic;
      FSL_S_Read       : out std_logic;
      FSL_S_Exists     : in  std_logic
    );
  end component if_decode;

  component fsl_v20 is
    generic (
      C_ASYNC_CLKS        : integer := 0;
      C_NUM_SYNC_FF       : integer := 2;
      C_IMPL_STYLE        : integer := 0;
      C_FSL_DWIDTH        : integer := 32;
      C_FSL_DEPTH         : integer := 16;
      C_FIFO_LENGTH_WIDTH : integer := 4
    );
    port (
      -- Clock and reset signals
      FSL_Clk : in  std_logic;
      SYS_Rst : in  std_logic;
      FSL_Rst : out std_logic;
  
      -- FSL master signals
      FSL_M_Clk     : in  std_logic;
      FSL_M_Data    : in  std_logic_vector(0 to C_FSL_DWIDTH-1);
      FSL_M_Control : in  std_logic;
      FSL_M_Write   : in  std_logic;
      FSL_M_Full    : out std_logic;
  
      -- FSL slave signals
      FSL_S_Clk     : in  std_logic;
      FSL_S_Data    : out std_logic_vector(0 to C_FSL_DWIDTH-1);
      FSL_S_Control : out std_logic;
      FSL_S_Read    : in  std_logic;
      FSL_S_Exists  : out std_logic;
  
      -- FIFO status signals
      FSL_M_Length    : out std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
      FSL_S_Length    : out std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
      FSL_Full        : out std_logic;
      FSL_Has_Data    : out std_logic;
      FSL_Control_IRQ : out std_logic
      );
  end component fsl_v20;
  
  
  -----------------------------------------------------------------------------
  -- Signal declaration
  -----------------------------------------------------------------------------
  -- Common FSL Clock and reset signals.
  signal FSL_Clk_I                : std_logic;
  signal SYS_Rst_Input            : std_logic:= '0';
  signal SYS_Rst_If0              : std_logic:= '0';
  signal SYS_Rst_If1              : std_logic:= '0';
  signal SYS_Rst_If0_n            : std_logic:= '0';
  signal SYS_Rst_If1_n            : std_logic:= '0';
  signal SYS_Rst_I                : std_logic:= '0';
  signal if0_rst                  : std_logic:= '0';
  signal if1_rst                  : std_logic:= '0';
  
  -- FSL interface #0 master/slave signals.
  signal FSL0_M_Clk_I             : std_logic;
  signal FSL0_M_Data_I            : std_logic_vector(0 to C_FSL_DWIDTH-1);
  signal FSL0_M_Control_I         : std_logic;
  signal FSL0_M_Write_I           : std_logic;
  signal FSL0_M_Full_I            : std_logic;
  signal FSL0_M_Length_I          : std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
  
  signal FSL0_S_Clk_I             : std_logic;
  signal FSL0_S_Data_I            : std_logic_vector(0 to C_FSL_DWIDTH-1);
  signal FSL0_S_Control_I         : std_logic;
  signal FSL0_S_Read_I            : std_logic;
  signal FSL0_S_Exists_I          : std_logic;
  signal FSL0_S_Length_I          : std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
  
  signal FSL0_Has_Data_I          : std_logic;
  
  -- FSL interface #0 master/slave signals.
  signal FSL1_M_Clk_I             : std_logic;
  signal FSL1_M_Data_I            : std_logic_vector(0 to C_FSL_DWIDTH-1);
  signal FSL1_M_Control_I         : std_logic;
  signal FSL1_M_Write_I           : std_logic;
  signal FSL1_M_Full_I            : std_logic;
  signal FSL1_M_Length_I          : std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
  
  signal FSL1_S_Clk_I             : std_logic;
  signal FSL1_S_Data_I            : std_logic_vector(0 to C_FSL_DWIDTH-1);
  signal FSL1_S_Control_I         : std_logic;
  signal FSL1_S_Read_I            : std_logic;
  signal FSL1_S_Exists_I          : std_logic;
  signal FSL1_S_Length_I          : std_logic_vector(0 to C_FIFO_LENGTH_WIDTH);
  
  signal FSL1_Has_Data_I          : std_logic;
    
begin  -- architecture IMP


  -----------------------------------------------------------------------------
  -- Assign FSL clock net
  -----------------------------------------------------------------------------
  -- The FSL_Clk is ignored for asynchronous connections.
  -- => C_ASYNC_CLKS doesn't need to be taken into account here.
  Using_AXI0_4_FSL: if( C_USE_AXI0 ) generate
  begin
    -- Interface #0 is AXI4, use that clock for the internal FSL links.
    FSL_Clk_I <= S0_AXI_ACLK;
  end generate Using_AXI0_4_FSL;
  
  Using_AXIS0_4_FSL: if( C_USE_AXIS0 ) generate
  begin
    -- Interface #0 is AXI4-Stream, use that clock for the internal FSL links.
    FSL_Clk_I <= S0_AXIS_ACLK;
  end generate Using_AXIS0_4_FSL;
  

  -----------------------------------------------------------------------------
  -- Select Reset 
  -----------------------------------------------------------------------------
  Using_AXI0_4_Rst: if( C_USE_AXI0 ) generate
  begin
    -- Interface #0 is AXI4, use that reset.
    if0_rst <= not S0_AXI_ARESETN;
  end generate Using_AXI0_4_Rst;
  
  Using_No0_4_Rst: if( not (C_USE_AXI0) ) generate
  begin
    -- Interface #0 is streaming, no dedicated reset.
    if0_rst <= '0';
  end generate Using_No0_4_Rst;
  
  Using_AXI1_4_Rst: if( C_USE_AXI1 ) generate
  begin
    -- Interface #1 is AXI4, use that reset.
    if1_rst <= not S1_AXI_ARESETN;
  end generate Using_AXI1_4_Rst;
  
  Using_No1_4_Rst: if( not (C_USE_AXI1) ) generate
  begin
    -- Interface #1 is streaming, no dedicated reset.
    if1_rst <= '0';
  end generate Using_No1_4_Rst;
  
  
  -----------------------------------------------------------------------------
  -- Assign Reset net
  -----------------------------------------------------------------------------
  -- Translate the SYS_Rst signal to be active high.
  Sys_Rst_Proc : process (SYS_Rst) is
  begin
    if C_EXT_RESET_HIGH = 0 then
      SYS_Rst_Input <= not SYS_Rst;
    else
      SYS_Rst_Input <= SYS_Rst;
    end if;
  end process Sys_Rst_Proc;

  -- Assign reset net depending on the selected interfaces.
  Rst_Async: if (C_ASYNC_CLKS /= 0) generate
    signal Bus0_Rst_d1               : std_logic:= '0';
    signal Bus0_Rst_d2               : std_logic:= '0';
    signal Bus1_Rst_d1               : std_logic:= '0';
    signal Bus1_Rst_d2               : std_logic:= '0';
    signal SYS_Rst_Input0_d1         : std_logic:= '0';
    signal SYS_Rst_Input0_d2         : std_logic:= '0';
    signal SYS_Rst_Input1_d1         : std_logic:= '0';
    signal SYS_Rst_Input1_d2         : std_logic:= '0';
  
    attribute ASYNC_REG                         : string;
    attribute ASYNC_REG of SYS_RST_FF_I0_1     : label is "TRUE";
    attribute ASYNC_REG of SYS_RST_FF_I0_2     : label is "TRUE";
    attribute ASYNC_REG of Bus_RST_FF_I0_1     : label is "TRUE";
    attribute ASYNC_REG of Bus_RST_FF_I0_2     : label is "TRUE";
    attribute ASYNC_REG of SYS_RST_FF_I1_1     : label is "TRUE";
    attribute ASYNC_REG of SYS_RST_FF_I1_2     : label is "TRUE";
    attribute ASYNC_REG of Bus_RST_FF_I1_1     : label is "TRUE";
    attribute ASYNC_REG of Bus_RST_FF_I1_2     : label is "TRUE";
    
  begin
    SYS_RST_FF_I0_1 : FD
      port map (
        Q => SYS_Rst_Input0_d1,
        D => SYS_Rst_Input,
        C => FSL0_M_Clk_I);
        
    SYS_RST_FF_I0_2 : FD
      port map (
        Q => SYS_Rst_Input0_d2,
        D => SYS_Rst_Input0_d1,
        C => FSL0_M_Clk_I);
        
    Bus_RST_FF_I0_1 : FD
      port map (
        Q => Bus1_Rst_d1,
        D => if1_rst,
        C => FSL0_M_Clk_I);
        
    Bus_RST_FF_I0_2 : FD
      port map (
        Q => Bus1_Rst_d2,
        D => Bus1_Rst_d1,
        C => FSL0_M_Clk_I);
          
    Create_Sys_Rst0: process(FSL0_M_Clk_I)
    begin
      if( FSL0_M_Clk_I'event and FSL0_M_Clk_I = '1' ) then
        -- Synchronize to clock.
        
        SYS_Rst_If0      <= if0_rst or Bus1_Rst_d2 or SYS_Rst_Input0_d2;
      end if;
    end process Create_Sys_Rst0;
    
    SYS_RST_FF_I1_1 : FD
      port map (
        Q => SYS_Rst_Input1_d1,
        D => SYS_Rst_Input,
        C => FSL1_M_Clk_I);
        
    SYS_RST_FF_I1_2 : FD
      port map (
        Q => SYS_Rst_Input1_d2,
        D => SYS_Rst_Input1_d1,
        C => FSL1_M_Clk_I);
        
    Bus_RST_FF_I1_1 : FD
      port map (
        Q => Bus0_Rst_d1,
        D => if0_rst,
        C => FSL1_M_Clk_I);
        
    Bus_RST_FF_I1_2 : FD
      port map (
        Q => Bus0_Rst_d2,
        D => Bus0_Rst_d1,
        C => FSL1_M_Clk_I);
          
    Create_Sys_Rst1: process(FSL1_M_Clk_I)
    begin
      if( FSL1_M_Clk_I'event and FSL1_M_Clk_I = '1' ) then
        -- Synchronize to clock.
        
        SYS_Rst_If1      <= if1_rst or Bus0_Rst_d2 or SYS_Rst_Input1_d2;
      end if;
    end process Create_Sys_Rst1;
    
    -- Asynchronous reset for FIFOs.
    SYS_Rst_I        <= SYS_Rst_Input or if0_rst or if1_rst;
    
  end generate Rst_Async;

  Rst_Sync: if (C_ASYNC_CLKS = 0) generate
    signal SYS_Rst_Input_d1         : std_logic:= '0';
    signal SYS_Rst_Input_d2         : std_logic:= '0';
  
    attribute ASYNC_REG                         : string;
    attribute ASYNC_REG of SYS_Rst_Input_d1     : signal is "TRUE";
    attribute ASYNC_REG of SYS_Rst_Input_d2     : signal is "TRUE";
    
  begin
    Create_Sys_Rst: process(FSL_Clk_I)
    begin
      if( FSL_Clk_I'event and FSL_Clk_I = '1' ) then
        -- Synchronize to clock.
        SYS_Rst_Input_d1 <= SYS_Rst_Input;
        SYS_Rst_Input_d2 <= SYS_Rst_Input_d1;
        
        SYS_Rst_I        <= SYS_Rst_Input_d2 or if0_rst or if1_rst;
      end if;
    end process Create_Sys_Rst;
    
    SYS_Rst_If0 <= SYS_Rst_I;
    SYS_Rst_If1 <= SYS_Rst_I;
      
  end generate Rst_Sync;
  
  SYS_Rst_If0_n <= not SYS_Rst_If0;
  SYS_Rst_If1_n <= not SYS_Rst_If1;
  
  
  -----------------------------------------------------------------------------
  -- Instantiating MAILBOX Interface #0
  -----------------------------------------------------------------------------
  Using_Bus_0: if C_USE_BUS0 generate
  begin
    Bus0_If: if_decode
      generic map(
        -- General generics.
        C_FAMILY                => C_FAMILY,
        C_ENABLE_BUS_ERROR      => C_ENABLE_BUS_ERROR,
        C_FIFO_LENGTH_WIDTH     => C_FIFO_LENGTH_WIDTH,
        C_INTERCONNECT          => C_INTERCONNECT_PORT_0,
        
        -- FSL generics
        C_FSL_DWIDTH            => C_FSL_DWIDTH,
        
        -- AXI4-Lite slave generics
        C_S_AXI_BASEADDR        => C_S0_AXI_BASEADDR,
        C_S_AXI_HIGHADDR        => C_S0_AXI_HIGHADDR,
        C_S_AXI_ADDR_WIDTH      => C_S0_AXI_ADDR_WIDTH,
        C_S_AXI_DATA_WIDTH      => C_S0_AXI_DATA_WIDTH
      )
      port map(
        Rst              => SYS_Rst_If0,

        -- AXI4-Lite SLAVE SINGLE INTERFACE
        S_AXI_ACLK       => S0_AXI_ACLK,
        S_AXI_ARESETN    => SYS_Rst_If0_n,
        S_AXI_AWADDR     => S0_AXI_AWADDR,
        S_AXI_AWVALID    => S0_AXI_AWVALID,
        S_AXI_AWREADY    => S0_AXI_AWREADY,
        S_AXI_WDATA      => S0_AXI_WDATA,
        S_AXI_WSTRB      => S0_AXI_WSTRB,
        S_AXI_WVALID     => S0_AXI_WVALID,
        S_AXI_WREADY     => S0_AXI_WREADY,
        S_AXI_BRESP      => S0_AXI_BRESP,
        S_AXI_BVALID     => S0_AXI_BVALID,
        S_AXI_BREADY     => S0_AXI_BREADY,
        S_AXI_ARADDR     => S0_AXI_ARADDR,
        S_AXI_ARVALID    => S0_AXI_ARVALID,
        S_AXI_ARREADY    => S0_AXI_ARREADY,
        S_AXI_RDATA      => S0_AXI_RDATA,
        S_AXI_RRESP      => S0_AXI_RRESP,
        S_AXI_RVALID     => S0_AXI_RVALID,
        S_AXI_RREADY     => S0_AXI_RREADY,
        
        -- Information signals.
        FSL_M_Length     => FSL0_M_Length_I,
        FSL_S_Length     => FSL0_S_Length_I,
        Interrupt        => Interrupt_0,
        
        -- FSL master/slave interface signals.
        FSL_M_Clk        => FSL0_M_Clk_I,
        FSL_M_Data       => FSL0_M_Data_I,
        FSL_M_Control    => FSL0_M_Control_I,
        FSL_M_Write      => FSL0_M_Write_I,
        FSL_M_Full       => FSL0_M_Full_I,
        
        FSL_S_Clk        => FSL0_S_Clk_I,
        FSL_S_Data       => FSL0_S_Data_I,
        FSL_S_Control    => FSL0_S_Control_I,
        FSL_S_Read       => FSL0_S_Read_I,
        FSL_S_Exists     => FSL0_S_Exists_I
      );
      
  end generate Using_Bus_0;
  No_If_0: if not C_USE_BUS0 generate
  begin
    -- Turn off unused.
    S0_AXI_AWREADY    <= '0';
    S0_AXI_WREADY     <= '0';
    S0_AXI_BRESP      <= (others=>'0');
    S0_AXI_BVALID     <= '0';
    S0_AXI_ARREADY    <= '0';
    S0_AXI_RDATA      <= (others=>'0');
    S0_AXI_RRESP      <= (others=>'0');
    S0_AXI_RVALID     <= '0';
  end generate No_If_0;
  
  Using_AXIS_0: if C_USE_AXIS0 generate
  begin
    -- Incoming data on interface #0.
    FSL0_M_Clk_I      <= S0_AXIS_ACLK;
    FSL0_M_Data_I     <= S0_AXIS_TDATA;
    FSL0_M_Control_I  <= S0_AXIS_TLAST;
    FSL0_M_Write_I    <= S0_AXIS_TVALID and not FSL0_M_Full_I;
    S0_AXIS_TREADY    <= not FSL0_M_Full_I;
    
    -- Outgoing data on interface #0.
    FSL0_S_Clk_I      <= M0_AXIS_ACLK;
    FSL0_S_Read_I     <= FSL0_S_Exists_I and M0_AXIS_TREADY;
    M0_AXIS_TDATA     <= FSL0_S_Data_I;
    M0_AXIS_TLAST     <= FSL0_S_Control_I;
    M0_AXIS_TVALID    <= FSL0_S_Exists_I;
    
    -- Interrrupt for interface #0.
    Interrupt_0       <= FSL0_Has_Data_I;
    
  end generate Using_AXIS_0;
  No_AXIS_0: if not C_USE_AXIS0  generate
  begin
    -- Turn off unused.
    S0_AXIS_TREADY    <= '0';
    M0_AXIS_TDATA     <= (others=>'0');
    M0_AXIS_TLAST     <= '0';
    M0_AXIS_TVALID    <= '0';
    
  end generate No_AXIS_0;
  
  
  -----------------------------------------------------------------------------
  -- Instantiating MAILBOX Interface #1
  -----------------------------------------------------------------------------
  Using_Bus_1: if C_USE_BUS1 generate
  begin
    Bus1_If: if_decode
      generic map(
        -- General generics.
        C_FAMILY                => C_FAMILY,
        C_ENABLE_BUS_ERROR      => C_ENABLE_BUS_ERROR,
        C_FIFO_LENGTH_WIDTH     => C_FIFO_LENGTH_WIDTH,
        C_INTERCONNECT          => C_INTERCONNECT_PORT_1,
        
        -- FSL generics
        C_FSL_DWIDTH            => C_FSL_DWIDTH,
        
        -- AXI4-Lite slave generics
        C_S_AXI_BASEADDR        => C_S1_AXI_BASEADDR,
        C_S_AXI_HIGHADDR        => C_S1_AXI_HIGHADDR,
        C_S_AXI_ADDR_WIDTH      => C_S1_AXI_ADDR_WIDTH,
        C_S_AXI_DATA_WIDTH      => C_S1_AXI_DATA_WIDTH
      )
      port map(
        Rst              => SYS_Rst_If1,

        -- AXI4-Lite SLAVE SINGLE INTERFACE
        S_AXI_ACLK       => S1_AXI_ACLK,
        S_AXI_ARESETN    => SYS_Rst_If1_n,
        S_AXI_AWADDR     => S1_AXI_AWADDR,
        S_AXI_AWVALID    => S1_AXI_AWVALID,
        S_AXI_AWREADY    => S1_AXI_AWREADY,
        S_AXI_WDATA      => S1_AXI_WDATA,
        S_AXI_WSTRB      => S1_AXI_WSTRB,
        S_AXI_WVALID     => S1_AXI_WVALID,
        S_AXI_WREADY     => S1_AXI_WREADY,
        S_AXI_BRESP      => S1_AXI_BRESP,
        S_AXI_BVALID     => S1_AXI_BVALID,
        S_AXI_BREADY     => S1_AXI_BREADY,
        S_AXI_ARADDR     => S1_AXI_ARADDR,
        S_AXI_ARVALID    => S1_AXI_ARVALID,
        S_AXI_ARREADY    => S1_AXI_ARREADY,
        S_AXI_RDATA      => S1_AXI_RDATA,
        S_AXI_RRESP      => S1_AXI_RRESP,
        S_AXI_RVALID     => S1_AXI_RVALID,
        S_AXI_RREADY     => S1_AXI_RREADY,
        
        -- Information signals.
        FSL_M_Length     => FSL1_M_Length_I,
        FSL_S_Length     => FSL1_S_Length_I,
        Interrupt        => Interrupt_1,
        
        -- FSL master/slave interface signals.
        FSL_M_Clk        => FSL1_M_Clk_I,
        FSL_M_Data       => FSL1_M_Data_I,
        FSL_M_Control    => FSL1_M_Control_I,
        FSL_M_Write      => FSL1_M_Write_I,
        FSL_M_Full       => FSL1_M_Full_I,
        
        FSL_S_Clk        => FSL1_S_Clk_I,
        FSL_S_Data       => FSL1_S_Data_I,
        FSL_S_Control    => FSL1_S_Control_I,
        FSL_S_Read       => FSL1_S_Read_I,
        FSL_S_Exists     => FSL1_S_Exists_I
      );
  
  end generate Using_Bus_1;
  No_If_1: if not C_USE_BUS1 generate
  begin
    -- Turn off unused.
    S1_AXI_AWREADY    <= '0';
    S1_AXI_WREADY     <= '0';
    S1_AXI_BRESP      <= (others=>'0');
    S1_AXI_BVALID     <= '0';
    S1_AXI_ARREADY    <= '0';
    S1_AXI_RDATA      <= (others=>'0');
    S1_AXI_RRESP      <= (others=>'0');
    S1_AXI_RVALID     <= '0';
  end generate No_If_1;
  
  Using_AXIS_1: if C_USE_AXIS1 generate
  begin
    -- Incoming data on interface #1.
    FSL1_M_Clk_I      <= S1_AXIS_ACLK;
    FSL1_M_Data_I     <= S1_AXIS_TDATA;
    FSL1_M_Control_I  <= S1_AXIS_TLAST;
    FSL1_M_Write_I    <= S1_AXIS_TVALID and not FSL1_M_Full_I;
    S1_AXIS_TREADY    <= not FSL1_M_Full_I;
    
    -- Outgoing data on interface #1.
    FSL1_S_Clk_I      <= M1_AXIS_ACLK;
    FSL1_S_Read_I     <= FSL1_S_Exists_I and M1_AXIS_TREADY;
    M1_AXIS_TDATA     <= FSL1_S_Data_I;
    M1_AXIS_TLAST     <= FSL1_S_Control_I;
    M1_AXIS_TVALID    <= FSL1_S_Exists_I;
    
    -- Interrrupt for interface #1.
    Interrupt_1       <= FSL1_Has_Data_I;
    
  end generate Using_AXIS_1;
  No_AXIS_1: if not C_USE_AXIS1 generate
  begin
    -- Turn off unused.
    S1_AXIS_TREADY    <= '0';
    M1_AXIS_TDATA     <= (others=>'0');
    M1_AXIS_TLAST     <= '0';
    M1_AXIS_TVALID    <= '0';
    
  end generate No_AXIS_1;
  
  
  -----------------------------------------------------------------------------
  -- Instantiating MAILBOX channel (If #0 -> If #1)
  -----------------------------------------------------------------------------
  fsl_0_to_1: fsl_v20
    generic map (
      C_ASYNC_CLKS        => C_ASYNC_CLKS,
      C_NUM_SYNC_FF       => C_NUM_SYNC_FF,
      C_IMPL_STYLE        => C_IMPL_STYLE,
      C_FSL_DWIDTH        => C_FSL_DWIDTH,
      C_FSL_DEPTH         => C_MAILBOX_DEPTH,
      C_FIFO_LENGTH_WIDTH => C_FIFO_LENGTH_WIDTH
    )
    port map (
      -- Clock and reset signals
      FSL_Clk           => FSL_Clk_I,
      SYS_Rst           => SYS_Rst_I,
      FSL_Rst           => open,
  
      -- FSL master signals
      FSL_M_Clk         => FSL0_M_Clk_I,
      FSL_M_Data        => FSL0_M_Data_I,
      FSL_M_Control     => FSL0_M_Control_I,
      FSL_M_Write       => FSL0_M_Write_I,
      FSL_M_Full        => FSL0_M_Full_I,
  
      -- FSL slave signals
      FSL_S_Clk         => FSL1_S_Clk_I,
      FSL_S_Data        => FSL1_S_Data_I,
      FSL_S_Control     => FSL1_S_Control_I,
      FSL_S_Read        => FSL1_S_Read_I,
      FSL_S_Exists      => FSL1_S_Exists_I,
  
      -- FIFO status signals
      FSL_M_Length      => FSL0_M_Length_I,
      FSL_S_Length      => FSL1_S_Length_I,
      FSL_Full          => open,
      FSL_Has_Data      => FSL1_Has_Data_I,
      FSL_Control_IRQ   => open
    );
  
  
  -----------------------------------------------------------------------------
  -- Instantiating MAILBOX channel (If #1 -> If #0)
  -----------------------------------------------------------------------------
  fsl_1_to_0: fsl_v20
    generic map (
      C_ASYNC_CLKS        => C_ASYNC_CLKS,
      C_NUM_SYNC_FF       => C_NUM_SYNC_FF,
      C_IMPL_STYLE        => C_IMPL_STYLE,
      C_FSL_DWIDTH        => C_FSL_DWIDTH,
      C_FSL_DEPTH         => C_MAILBOX_DEPTH,
      C_FIFO_LENGTH_WIDTH => C_FIFO_LENGTH_WIDTH
    )
    port map (
      -- Clock and reset signals
      FSL_Clk           => FSL_Clk_I,
      SYS_Rst           => SYS_Rst_I,
      FSL_Rst           => open,
  
      -- FSL master signals
      FSL_M_Clk         => FSL1_M_Clk_I,
      FSL_M_Data        => FSL1_M_Data_I,
      FSL_M_Control     => FSL1_M_Control_I,
      FSL_M_Write       => FSL1_M_Write_I,
      FSL_M_Full        => FSL1_M_Full_I,
  
      -- FSL slave signals
      FSL_S_Clk         => FSL0_S_Clk_I,
      FSL_S_Data        => FSL0_S_Data_I,
      FSL_S_Control     => FSL0_S_Control_I,
      FSL_S_Read        => FSL0_S_Read_I,
      FSL_S_Exists      => FSL0_S_Exists_I,
  
      -- FIFO status signals
      FSL_M_Length      => FSL1_M_Length_I,
      FSL_S_Length      => FSL0_S_Length_I,
      FSL_Full          => open,
      FSL_Has_Data      => FSL0_Has_Data_I,
      FSL_Control_IRQ   => open
    );


end architecture IMP;



